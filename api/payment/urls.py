from django.urls import path, include
from . import views

urlpatterns = [
    path('gettoken/', views.generate_token),
    path('process/', views.process_payment),
]