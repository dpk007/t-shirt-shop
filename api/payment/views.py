from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from django.http import HttpResponse, JsonResponse
from rest_framework.permissions import IsAuthenticated
from django.views.decorators.csrf import csrf_exempt
import braintree

# Create your views here.


gateway = braintree.BraintreeGateway(
    braintree.Configuration(
        environment=braintree.Environment.Sandbox,
        merchant_id='625m8sw6h6dwsd2s',
        public_key='d9whygbt6cjy39qs',
        private_key='0e296c97e633925c4dff8a6126e8b724'
    )
)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def generate_token(request):
    return JsonResponse({'clientToken': gateway.client_token.generate(), 'success': True})


@csrf_exempt
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def process_payment(request):
    nonce_from_the_client = request.POST["paymentMethodNonce"]
    amount_from_the_client = request.POST["amount"]

    result = gateway.transaction.sale({
        "amount": amount_from_the_client,
        "payment_method_nonce": nonce_from_the_client,
        "options": {
            "submit_for_settlement": True
        }
    })

    if result.is_success:
        return JsonResponse({"success": result.is_success,
                             "transaction": {"id": result.transaction.id,
                                             "amount": result.transaction.amount}
                             })
    else:
        return JsonResponse({"error": True, "success": False})

