from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from .serializers import UserSerializer
from .models import CustomUser
from django.http import JsonResponse
from django.contrib.auth import get_user_model
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login, logout
from django.conf import settings
import re
import requests
import random


# Create your views here.


def generate_session_token(username, password):
    URL = f"{settings.API_URL}api-token-auth/"
    DATA = {'username': username, 'password': password}
    try:
        response = requests.post(url=URL, data=DATA)
        return response.json()['token']
    except Exception:
        return "".join(random.SystemRandom().choice([chr(i) for i in range(97, 123)] + [str(i) for i in range(10)]) for _ in
                   range(20))


@csrf_exempt
def signin(request):
    if not request.method == 'POST':
        return JsonResponse({'error': 'Send a post with valid parameter'})
    print(f"datarequest{request.POST}")
    username = request.POST['email']
    password = request.POST['password']

    if not re.match("^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$", username):
        return JsonResponse({'error': 'Enter a valid email'})

    if len(password) < 3:
        return JsonResponse({'error': 'Password must be 3 latter'})

    UserModel = get_user_model()

    try:
        user = UserModel.objects.get(email=username)

        if user.check_password(password):
            usr_dict = UserModel.objects.filter(email=username).values().first()
            usr_dict.pop('password')

            if user.session_token != "0":
                user.session_token = "0"
                user.save()
                return JsonResponse({'error': 'Previous session exist!'}
                                    )

            token = generate_session_token(username, password)
            user.session_token = token
            user.save()
            login(request, user)
            return JsonResponse({'token': token, 'user': usr_dict})
        else:
            return JsonResponse({'error': 'Password not correct.'})

    except UserModel.DoesNotExist:
        return JsonResponse({'error': 'Invalid user'})


def signout(request, id):
    logout(request)

    UserModel = get_user_model()

    try:
        user = UserModel.objects.get(pk=id)
        user.session_token = "0"
        user.save()
        return JsonResponse({'msg': 'Logout successfully'})
    except UserModel.DoesNotExist:
        return JsonResponse({'error': 'Invalid user id'})


class UserViewSet(viewsets.ModelViewSet):
    permission_class_by_action = {'create': [AllowAny]}

    queryset = CustomUser.objects.all().order_by('id')
    serializer_class = UserSerializer

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_class_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_class]
