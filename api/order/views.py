from rest_framework import viewsets
from django.http import JsonResponse
from django.contrib.auth import get_user_model
from .serializers import OrderSerializers
from .models import Order
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import action
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

# Create your views here.


@csrf_exempt
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def add(request):

    if request.method == 'POST':
        user_id = request.user
        transaction_id = request.POST['transaction_id']
        amount = request.POST['amount']
        products = request.POST['products']
        total_pro = len(products.split(','))
        UserModel = get_user_model()

        try:
            user = UserModel.objects.get(email=user_id)
        except UserModel.DoesNotExist:
            return JsonResponse({'error': 'User does not exist'})

        ordr = Order(user=user, product_names=products, total_amount=amount, total_products=total_pro,
                     transaction_id=transaction_id)
        ordr.save()
        return JsonResponse({'success': True, 'error': False, 'msg': 'Order stored successfully'})


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all().order_by('id')
    serializer_class = OrderSerializers

    @action(methods=['GET'], detail=False, url_path='get_user_order', url_name='get_user_order')
    def get_data_by_id(self, request: Request, *args, **kwargs):
        email = request.user
        UserModel = get_user_model()
        user = UserModel.objects.get(email=email)
        order_info = Order.objects.filter(user=user.id).values()
        return Response({'data': order_info})
