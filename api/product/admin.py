from django.contrib import admin
from api.product.models import Product


class ProductAdmin(admin.ModelAdmin):
    list_display = ("name", "price", "is_active", "stock")
    list_filter = ("category", "is_active")
    search_fields = ("name__startswith",)


# Register your models here.

admin.site.register(Product, ProductAdmin)
