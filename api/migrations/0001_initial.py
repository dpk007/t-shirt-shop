from django.db import migrations
from api.user.models import CustomUser


class Migration(migrations.Migration):
    def seed_data(apps, schema_editor):
        user = CustomUser(name="dharam",
                          email="dharambk@gmail.com",
                          is_staff=True,
                          is_superuser=True,
                          phone="9292929292",
                          gender="Male"
                          )
        user.set_password("dharam007")
        user.save()

    dependencies = []

    operations = [
            migrations.RunPython(seed_data),
        ]
