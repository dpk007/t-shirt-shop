# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a simple shopping web app.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up?
### Requirements:
 - Git.
 - Pyenv(pipenv) with Python 3.7.
 - Database (PostgreSQL 12.2 ).
 
1. clone or download repository.
2. Setup virtual environment using pipenv.
```
#install pipenv outside project
   pip3 install pipenv 
#Go into the repo and run bellow command to create virtual env.
   pipenv shell
```
3. Install all required packages.
```
# linux/macOS
  pipenv install

# windows
  pipenv install
```
4. Before running the app run migrate command for propagating changes you've made to your models (adding a field, deleting a model, etc.) into your database schema

```
# Linux/macOS
  python3.7 manage.py makemigrations
  python3.7 manage.py migrate

# Windows
py -3.7 manage.py makemigrations
py -3.7 manage.py migrate
```
5. Run the web server.
```
# Linux/macOS
python3.7 manage.py runserver

# Windows
py -3.7 manage.py runserver
```
For testing payment use below image dummy test cred.
![alt text](media/readme/payment.png)